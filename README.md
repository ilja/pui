# Pui

A qt [Pleroma](https://pleroma.social) micro blogging client for [Ubuntu Touch](https://ubports.com)

## Goal

The goal off this app is to have a Pleroma app for Ubuntu Touch focused on the micro blogging aspect of Pleroma. I believe in the "do one thing and do it good" proverb, so I want to make strict lines in the goal of this app. There should be a point where the app is 'done'. That doesn't mean that new version can't come out any more, but from that moment on, most should just be maintenance.

### What it does

* It starts with `clickable desktop` (not yet tested on phone)
* When no one is authenticated yet, you get a login screen and you can authenticate
* When something goes wrong during authentication, you get a general error message
* When someone was already logged in, you go to the Home Timeline page
* The Home Timeline shows posts
    * It shows repeats
    * It shows scope and who posted it
    * It shows if it's a reply
    * It shows the Subject when present
    * It shows the content
    * It shows number of replies, repeats and favourites
    * It shows when you repeated or favourited the post
    * It shows media
    * It does **not yet** show polls
* The Home Timeline only shows the posts it got when you went to the Home Timeline
* Log out works

### What's in scope

These are things I consider must haves for a simple microblog client for Pleroma. These are features I want myself and should eventualy be implemented.

* On the home timeline
    * Media when present
      * Including media descriptions
    * Polls
      * Poll options
* Show thread
* Be able to post
    * Scopes
* Be able to interact with posts
    * Reply
      * Rn I'm thinking of opening the thread, and under the post you're replying to, you can have the ComposePost thing. That way you can read the whole thread while replying.
    * Fav
    * Repeat
    * Vote
* light vs dark mode according to system settings
* Make it look good and cute

### What's not out of scope

These are things I consider nice to haves for a microblogging client for Pleroma. If I feel I miss them, I may implement them, but there's a chance I wont. If you make a fork with the changes, I may merge them back here.

* Notification indicator
* Extra timelines that are supported in the BE
    * Federated timeline
    * Local timeline
    * Notifications
    * Bookmarks
    * Lists
    * Submitted reports and responses
      * If/when BE supports this, see also https://git.pleroma.social/pleroma/pleroma/-/merge_requests/3280
* push notifications
    * UBport docs: https://docs.ubports.com/en/latest/appdev/guides/pushnotifications.html
    * Pleroma api: https://api.pleroma.social/#operation/SubscriptionController.create
* Extra post options
    * Subject line
    * HTML/Markdown/BBCode...
    * Media upload
      * Media description
      * Sensitive checkbox
    * emoji picker (needed for custom emojis)
    * Polls
    * Stickers (as done for Pleroma-fe)
* View profiles
    * Follow/Unfollow users
    * Ignore/Block/Report users
* Edit own profile
* Extra post interactions
    * Emoji reactions
    * Bookmark posts
    * Ignore
    * Report
* Proper OAuth flow using the browser instead of providing credentials directly in the app
* Search
* Certain settings that are relevant for the scope of this app
* Multiple accounts
* Small changes to allow it to work on other platforms than Pleroma
    * Note that these should remain small changes. If the API or functionality are too different, then maybe it requires it's own app.
    * A Mastodon native app for Ubuntu Touch was requested at https://forums.ubports.com/topic/4754/wish-list-which-apps-do-you-need/125
* Accept/refuse follow requests for locked accounts
* Translations
* An icon/logo
* Images throughout the app (like for error pages when there's no connection, login screen, etc.)
* Very simple Moderator things like being able to delete posts and get reports through notifications
* Build for other platforms than Ubuntu Touch

### What's out of scope

These are examples of thing I consider out of scope for this app. These go beyond what a Pleroma microblog client on phones should be and probably need their own app instead. You should consider these things that wont be added ever.

* Non micro-blog stuff like chats, media centered TL, etc.
    * These should probably just have their own app
* FE specific things
    * No extra word filters unless it can be done from the BE
    * No complex theming
    * ...
* More complex Admin/moderator stuff
* Big changes for supporting extra BE's

## Contributing

I'm mostly making the app for myself in the first place and I still have to figure out many things, but if you have requests or changes you made or bugs you found or you just want to let me know how awesome you think this app is, you can contact me on fedi, I'm [ilja@ilja.space](https://ilja.space/ilja).

## Forking

Even thoug I use a gplv3 license, I do want the rest of the app to be as [trivial](https://trivial.technology/landing.html) as possible. I don't only want this app to be usefull for myself, but also to be a way to empower other people to make their own versions of it, furthering their own goals.

A more in depth explanation on how to read and go through the code can be found in the READING.ms file of this repo.

## Installing/building

This was started from a basic template using [Clickable](https://github.com/bhdouglass/clickable). For version control I use [Git](https://git-scm.com/). The OS I use is [Kubuntu](https://kubuntu.org/).

To get the source using github

```sh
# Clone using Git
git clone https://codeberg.org/ilja/pui
# Change directory so you're in the folder
cd pui
```

Then you can run the app on desktop using

```sh
clickable desktop
```

To run on your phone, you should put your phone in developer mode, connect it to your PC and run

```sh
clickable
```

This will build the application, move it to your phone, install it there and start it up. Once installed, you can disconnect from your PC if you want. You can keep using the app. As far as your phone is conserned, it's installed as any other app.

## License
### Pui

Copyright (C) 2021  Ilja

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.

### Font Awesome Icon

The Font Awesome icons are [licensed under the Creative Commons Attribution 4.0](https://fontawesome.com/license/free).
