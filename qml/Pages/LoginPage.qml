// Project "pui"
import QtQuick 2.9
import QtQuick.Layouts 1.2
import Ubuntu.Components 1.3
import "../Models"

Page {
  anchors.fill: parent
  id: page
  
  function login (fullHandle, password) {
    log.cli("Login Button Clicked", log.level.DEBUG)
    labelError.text = ""
    let userArray = fullHandle.split("@").filter((e) => {return e != ""})
    let username = userArray[0]
    let instance = userArray[1]
    let app_key
    
    if (username === undefined || instance === undefined || password === "") {
      labelError.text = i18n.tr("Please provide valid credentials")
    } else {
      http.authenticate(instance, username, password, labelError)
    }
  }
  
  header: PageHeader {
    id: header
    title: i18n.tr('Pui')
  }

  ColumnLayout {
    anchors.top: header.bottom
    anchors.left: parent.left
    anchors.right: parent.right
    
    anchors.topMargin: units.gu(5)
    anchors.leftMargin: units.gu(5)
    anchors.rightMargin: units.gu(5)

    Label {
      id: labelUsername
      text: i18n.tr("Please provide your full handle")
    }

    TextField {
      id: textFieldUsername
      Layout.fillWidth: true
      placeholderText: i18n.tr("e.g. ilja@ilja.space")
    }
    
    Label {
      text: i18n.tr("Please provide your password")
    }

    TextField {
      id: textFieldPassword
      echoMode: TextInput.Password
      Layout.fillWidth: true
      placeholderText: "**************"
    }

    Button {
      id: buttonLogin
      Layout.fillWidth: true
      color: UbuntuColors.green
      text: i18n.tr("Log In")
      onClicked: login (textFieldUsername.text, textFieldPassword.text)
    }
    
    Label {
      id: "labelError"
      text: ""
      color: UbuntuColors.red // TODO light vs dark mode
    }
  }
}
