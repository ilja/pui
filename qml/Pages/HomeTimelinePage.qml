// Project "pui"
import QtQuick 2.9
import QtQml 2.2
import QtQuick.Layouts 1.2
import QtQuick.XmlListModel 2.0
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3
import "../Components"

Page {
  id: home_timeline
  anchors.fill: parent
  
  property var newest_post_id: "" // I should be able to do something like `model.get(0).id` instead but can't get it to work
  property var oldest_post_id: ""
  property var is_filled: false
  
  function fillTimeline() {
    model.clear ()
    
    http.fetchTimeline(
      // Succes
      (response) => {
        newest_post_id = response[0].id
        oldest_post_id = response[response.length - 1].id

        for (let i=0;i<response.length;i++) {
          model.append({
            post_data: response[i]
          })
        }
        is_filled = true
      },
      // Error
      (request) => {
        // TODO: Idk, show error screen or smthng?
        // 
      }
    )
  }

  header: PuiPageHeader {
    id: header
    title: i18n.tr('Home')
  }

  // See: https://api-docs.ubports.com/sdk/apps/qml/QtQuick/ListView.html#sdk-qtquick-listview
  // If we need multiple types, see https://stackoverflow.com/questions/49837018/how-to-create-a-qml-delegate-with-conditional-substructure-for-a-heterogenous-li
  // TODO: use pullToRefresh: I need to use an UbuntuListView, see https://api-docs.ubports.com/sdk/apps/qml/Ubuntu.Components/UbuntuListView.html#sdk-ubuntu-components-ubuntulistview , but the background behind the avatar on the first row trurns grey and I don't now why.
  ListView {
    anchors.top: header.bottom
    id: itemList
    width: parent.width
    height: parent.height - header.height
    onAtYEndChanged: {
      if (itemList.atYEnd && is_filled) {
        // Loading tail messages...
        http.fetchTimeline(
          // Succes
          (response) => {
        oldest_post_id = response[response.length - 1].id

        for (let i=0;i<response.length;i++) {
          model.append({
            post_data: response[i]
          })
        }
          },
          // Error
          (request) => {
            // TODO: Idk, show error screen or smthng?
            // 
          },
          undefined,
          oldest_post_id
        )
      }
    }
    delegate: PostItem {}
    model: ListModel { id: model }
    Component.onCompleted: fillTimeline()
  }
  
  function updateTimeline() {
    http.fetchTimeline(
      // Succes
      (response) => {
        if(response.length > 0) {
          newest_post_id = response[0].id
        }

        for (let i=0;i<response.length;i++) {
          model.append({
            post_data: response[i]
          })
        }
      },
      // Error
      (request) => {
        // TODO: Idk, show error screen or smthng?
        // 
      },
      newest_post_id
    )
  }

  Timer {
      interval: 5000
      running: true
      repeat: true
      onTriggered: updateTimeline()
  }
}
