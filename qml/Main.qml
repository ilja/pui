/*
 * Copyright (C) 2021  Ilja
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * pui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.9
import Ubuntu.Components 1.3
import "Models"

MainView {
  id: root
  objectName: 'mainView'
  applicationName: 'pui.ilja'
  automaticOrientation: true

  // Units are always set in Grid Units
  // That way our layout is independed from the resolution of the phones screen.
  width: units.gu(45)
  height: units.gu(75)

  property var application_name: applicationName

  // Here we define the models we'll use
  HttpModel { id: http }
  StoreModel { id: store }
  LogModel { id: log }
  
  // This is our pagestack. This allows us to navigate through the pages of our application
  PageStack { id: mainStack }
  
  /**
   * This function is called from the StoreModel
   * The reason is that we can't decide what page to use as first page until we know if there's a token in the store
   */
  function setFirstPage(user_token) {
    
    http.setUserToken(user_token)
    mainStack.clear()
        
    if (user_token.instance && user_token.username && user_token.access_token) {
      mainStack.push(Qt.resolvedUrl("Pages/HomeTimelinePage.qml"))
    } else {
      mainStack.push(Qt.resolvedUrl("Pages/LoginPage.qml"))
    }
  }
}
