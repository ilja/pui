import Ubuntu.Components 1.3

// See https://api-docs.ubports.com/sdk/apps/qml/Ubuntu.Components/PageHeader.html
PageHeader {
    id: header
    title: ""
    trailingActionBar {
        // Here's a list of actions. When there are too many actions to show on screen, it will create a drop down.
        // You can set a "numberOfSlots" param if you only want a max amount of Actions shown.
        // For icon, you can use theme-icons provided by UT. See `cat /usr/share/icons/suru/suru-icon-theme.json`
        // I prefer using UT icons, but if none are available for the Action in question, you can use `iconSource` to set a custom icon.
        actions: [
            Action {
                iconSource: "../../assets/right-from-bracket-solid.svg"
                text: i18n.tr("Log out")
                onTriggered: {
                    http.revoke_login()
                    store.deleteUserToken(http.user_token.instance)
                    store.initialise_pui()
                }
            }
       ]
    }
  }
