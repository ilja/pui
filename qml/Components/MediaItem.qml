import QtQuick 2.9
import QtQml 2.9
import QtQuick.Layouts 1.2
import QtQuick.Particles 2.9
import Ubuntu.Components 1.3

ListItem {
  id: mediaItem
  implicitHeight: main_column.height
  height: media_item.height
  width: parent.width
  
  ColumnLayout {
    id: main_column
    height: parent.height
    width: parent.width
    Label {
      id: media_unknown
      visible: ! media_item.is_image
      height: parent.height
      width: parent.width
      text: '<a href="' + media_item.remote_url + '" >' + media_item.remote_url +'</a>'
      onLinkActivated: Qt.openUrlExternally(link)
      wrapMode: Text.Wrap
    }
    Image {
      id: media_image
      visible: media_item.is_image
      height: parent.height
      width: parent.width
      sourceSize.height: media_item.height
      source: (media_item.is_image ? media_item.preview_url : "")
      fillMode: Image.PreserveAspectFit
      
    }
  }
  
   Component.onCompleted: () => {
     console.log(JSON.stringify(media_item))
   }
}
