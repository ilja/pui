import QtQuick 2.9
import QtQml 2.9
import QtQuick.Layouts 1.2
import QtQuick.Particles 2.9
import Ubuntu.Components 1.3

/**
 * +---------------------------------------------------+
 * | reblog                                            |
 * +---------+-----------------------------------------+
 * | block1  |block2+--------+----------+-------------+|
 * |+------+ |      |name    |username  |   time scope||
 * ||avatar| |      +--------+----------+-------------+|
 * ||      | |      |in_reply_to                      ||
 * ||      | |      +---------------------------------+|
 * |+------+ |      |subject                          ||
 * |         |      +---------------------------------+|
 * |         |      |post_content                     ||
 * |         |      |                                 ||
 * |         |      |                                 ||
 * |         |      +---------------------------------+|
 * |         |      |media                            ||
 * |         |      +---------------------------------+|
 * |         |      |action_bar                       ||
 * |         |      +---------------------------------+|
 * +---------+-----------------------------------------+
 * |                  Polls                            | TODO
 * +---------------------------------------------------+
 */
  
ListItem {
  id: postItem
  implicitHeight: main_column.height

  function action_icon(post_data, key_action_self, icon_inactive, icon_active) {
    return (post_data[key_action_self] ? icon_active : icon_inactive)
  }

  function action_label(post_data, key_action_count) {
    return (post_data[key_action_count] === 0 ? "" : post_data[key_action_count])
  }
  
  function scope_source(visibility) {
    let icon = {
        "direct": "../../assets/envelope-solid.svg",
        "private": "../../assets/lock-solid.svg",
        "unlisted": "../../assets/lock-open-solid.svg",
        "public": "../../assets/globe-solid.svg"
    }
    
    return icon[visibility]
  }
    
  function time_string(created_at) {
    let time_diff = Math.round(new Date().getTime()/1000 - new Date(created_at).getTime()/1000)
    
    let seconds_per = {
      minute: 60,
      hour: 3600,
      day: 86400,
      week: 604800,
      month: 2592000,
      year: 31536000
    }
    
    if (time_diff < seconds_per.minute) {
      time_diff = time_diff + i18n.tr("s")
    } else if (time_diff < seconds_per.hour) {
      time_diff = Math.round(time_diff/seconds_per.minute) + i18n.tr("min")
    } else if (time_diff < seconds_per.day) {
      time_diff = Math.round(time_diff/seconds_per.hour) + i18n.tr("h")
    } else if (time_diff < seconds_per.week) {
      time_diff = Math.round(time_diff/seconds_per.day) + i18n.tr("d")
    } else if (time_diff < seconds_per.month) {
      time_diff = Math.round(time_diff/seconds_per.week) + i18n.tr("w")
    } else if (time_diff < seconds_per.year) {
      time_diff = Math.round(time_diff/seconds_per.month) + i18n.tr("mo")
    } else {
      time_diff = Math.round(time_diff/seconds_per.year) + i18n.tr("y")
    }
    
    return time_diff
  }
  
  function shorten_string(string, length) {
    // TODO I find it hard to believe I wouldn't be able to do this better with QML
    if (! length) {
      length=13
    }
    return (string.length > length ? string.substring(0, length-3) + "..." : string)
  }
  
  function media_list_height(media_attachements) {
    let height=0
    for (let i=0;i<media_attachements.length;i++) {
      height += media_attachements[i].height
    }
    return height
  }
          
  ColumnLayout {
    id: main_column

    RowLayout {
      id: reblog_row
      visible: post_data.is_reblog
      Rectangle {
        id: reblog_avatar
        implicitHeight: reblog_avatar_image.height
        width: units.gu(10)
        Image {
          id: reblog_avatar_image
          anchors.right: parent.right
          sourceSize.width: units.gu(3)
          sourceSize.height: units.gu(3)
          source: post_data.reblog.account.avatar
          fillMode: Image.PreserveAspectFit
        }
      }
      Label {
        id: reblog_user_name
        text: shorten_string(post_data.reblog.account.username, 15)
        font.bold: true
      }
      Image {
        id: reblogged_icon
        sourceSize.width: units.gu(1.5)
        sourceSize.height: units.gu(1.5)
        source: "../../assets/retweet-solid.svg"
        fillMode: Image.PreserveAspectFit
      }
      Label {
        id: rebloged_label
        opacity: 0.5
        text: i18n.tr("repeated")
      }
    }

    RowLayout {
      id: post_item_row
      implicitHeight: Math.max(avatar.height, block2.height)
      
      Rectangle {
        id: block1
        width: units.gu(10)
        Layout.alignment: Qt.AlignTop
        Image {
          id: avatar
          sourceSize.width: units.gu(10)
          sourceSize.height: units.gu(10)
          source: post_data.note.account.avatar
          fillMode: Image.PreserveAspectFit
        }
      }
  
      Rectangle {
        id: block2
        width: root.width - block1.width
        implicitHeight: block2_column.height
        Layout.alignment: Qt.AlignTop
        
        ColumnLayout {
          id: block2_column
          width: block2.width
          implicitHeight: block2_top_row.height + post_row.height + media_list.height + action_row.height
          
          Rectangle {
            id: block2_top_row
            width: block2.width
            height: block2_top_row_first_column.height
            RowLayout {
              Rectangle {
                id: block2_top_row_first_column
                width: display_name.width
                height: display_name.height
                Label {
                  id: display_name
                  font.bold: true
                  text: shorten_string(post_data.note.account.display_name)
                }
              }
              Rectangle {
                id: block2_top_row_second_column
                // TODO: Why do I need this ofset? It should be possible to get this better
                width: block2_top_row.width - block2_top_row_first_column.width - block2_top_row_third_column.width - units.gu(4)
                height: username.height
                Label {
                  id: username
                  text: shorten_string(post_data.note.account.username,10)
                }
              }
              Rectangle {
                id: block2_top_row_third_column
                width: time_label.width + scope_icon.width
                height: Math.max(time_label.height, scope_icon.height)
                RowLayout {
                  Label {
                    id: time_label
                    text: time_string(post_data.created_at)
                  }
                  Image {
                    id: scope_icon
                    width: units.gu(2)
                    sourceSize.width: units.gu(2)
                    sourceSize.height: units.gu(2)
                    source: scope_source(post_data.visibility)
                    fillMode: Image.PreserveAspectFit
                  }
                }
              }
            }
          }
          
          RowLayout {
            id: in_reply_to
            width: block2.width
            visible: post_data.is_in_reply_to
            Image {
              id: in_reply_to_icon
              sourceSize.width: units.gu(1.5)
              sourceSize.height: units.gu(1.5)
              source: "../../assets/reply-solid.svg"
              mirror: true
              fillMode: Image.PreserveAspectFit
            }
            Label {
              text: i18n.tr("in reply to")
            }
            Label {
              text: post_data.in_reply_to.account.username
            }
          }
          
          ColumnLayout {
            id: subject
            width: block2.width - units.gu(2)
            visible: post_data.note.content.has_subject
            Rectangle {
              width: subject.width
              height: subject_field.height
              Label {
                id: subject_field
                font.italic: true
                text: post_data.note.content.subject
              }
            }
            Rectangle {
              id: subject_underline
              width: subject.width
              height: units.gu(0.2)
              border.width: units.gu(0.2)
              opacity: 0.1
            }
          }
          Rectangle {
            id: post_row
            width: block2.width
            implicitHeight: Math.max(post_content.paintedHeight, avatar.sourceSize.height - block2_top_row.height - action_row.height)
            Label {
              id: post_content
              width: block2.width
              text: post_data.note.content.content
              wrapMode: Text.Wrap
              onLinkActivated: Qt.openUrlExternally(link)
            }
          }
          
          ListView {
            id: media_list
            visible: post_data.has_media
            width: parent.width
            height: media_list_height(post_data.media_attachements)
            delegate: MediaItem {}
            model: ListModel { id: model }
            Component.onCompleted: () => {
              let height=0
              for (let i=0;i<post_data.media_attachements.length;i++) {
                model.append({
                  media_item: post_data.media_attachements[i]
                })
              }
            }
          }
          
          
          // I shouldcheck how multiple media works now. Is everything under one list, or are video, image, other stuff... in seperate lists and what's the order of things? It's not in the order as uploaded, but I will put it in that order because it seems easier.
          // TODO: Media
          
  //             Image {
  //               id: media
  //               sourceSize.width: units.gu(45)
  //               //sourceSize.height: units.gu(2)
  //               source: post_data.media_attachements[0].remote_url
  //               fillMode: Image.PreserveAspectFit
  //             }
          
      //     ListView {
      //       id: media
      //       visible: post_data.has_media
      //       delegate: ImageItem {}
      //       model: ListModel { id: media_list }
      //       Component.onCompleted: () => {
      //         
      //         media_list.append({attachement: post_data.media[0]})
      //         
      //       }
      //     }
          
          Rectangle {
            id: action_row
            width: block2.width
            height: units.gu(3)
            RowLayout {
              Rectangle {
                id: action_row_first_column
                width: block2.width / 5
                RowLayout {
                  Image {
                    id: reply_icon
                    sourceSize.width: units.gu(2)
                    sourceSize.height: units.gu(2)
                    source: action_icon(
                      post_data, 
                      "null", 
                      "../../assets/reply-solid.svg", 
                      "../../assets/reply-solid.svg"
                    )
                    fillMode: Image.PreserveAspectFit
                  }
                  Label {
                    id: reply_label
                    text: action_label(post_data, "replies_count")
                  }
                }
              }
              Rectangle {
                id: action_row_second_column
                width: block2.width / 5
                RowLayout {
                  Image {
                    id: reblog_icon
                    sourceSize.width: units.gu(2)
                    sourceSize.height: units.gu(2)
                    source: action_icon(
                      post_data, 
                      "reblogged", 
                      "../../assets/retweet-solid.svg", 
                      "../../assets/retweet-solid-double.svg"
                    )
                    fillMode: Image.PreserveAspectFit
                  }
                  Label {
                    id: reblog_label
                    text: action_label(post_data, "reblogs_count")
                  }
                }
              }
              Rectangle {
                id: action_row_third_column
                width: block2.width / 5
                RowLayout {
                  Image {
                    id: favourite_icon
                    sourceSize.width: units.gu(2)
                    sourceSize.height: units.gu(2)
                    source: action_icon(
                      post_data, 
                      "favourited", 
                      "../../assets/star-regular.svg", 
                      "../../assets/star-solid.svg"
                    )
                    fillMode: Image.PreserveAspectFit
                  }
                  Label {
                    id: favourite_label
                    text: action_label(post_data, "favourites_count")
                  }
                }
              }
            }
          }
        }
      }
    }
    // TODO: Polls
  }
}
