import QtQuick 2.9
import QtQuick.LocalStorage 2.0

/**
 * Storage Model
 * 
 * This model is responsible for storing and retrieving things from Localstorage.
 * See https://doc.qt.io/qt-5/qtquick-localstorage-qmlmodule.html
 */

// TODO: For updating the Timeline, I should only keep so many posts in the Database. Not sure yet how exactly to handle it. Maybe start anew when more than X new posts were found in one call?
  
Item {
  id: store
  
  property var db: LocalStorage.openDatabaseSync("Pui", "0.1", "Pui Database", 1000000)
  
  /**
   * Shortener for the sqlite transactions
   * Copied and adapted from Fluffychat
   */
  function query (statement, values) {
    log.cli(statement, log.level.DEBUG)
    try {
      var rs = {}
      db.transaction(
        (tx) => {
          if (values) {
            rs = tx.executeSql(statement, values)
          } else {
            rs = tx.executeSql(statement)
          }
        }
      )
      return rs
    }
    catch (e) {
      log.cli(e + " " + statement, log.level.ERROR)
    }
  }

  /**
   * Does a select and returns results in a list of objects
   */
  function select(statement, values) {
    let rows = query(statement, []).rows
    
    let result = []
    for (let i=0; i < rows.length; i++) {
      result.push(rows.item(i))
    }
    log.cli(JSON.stringify(result), log.level.DEBUG)
    return result
  }
  
  /**
   * Shortener for when you only select the first result
   */
  function first (statement, values, fallback) {
    let rows = query(statement, values).rows
    if (rows.length > 0) {
      let result=rows.item(0)
      log.cli(JSON.stringify(result), log.level.DEBUG)
      return result
    }
    log.cli("No result found for '" + statement + "'", log.level.DEBUG)
    return fallback
  }

  /**
   * This initialises PUI
   * It creates the DB if it doesn't exists yet and sets the first page
   * Setting the first page is added in this function because it needs to be called from this module's `onCompleted` method
   */
  function initialise_pui() {
      query(
          'CREATE TABLE IF NOT EXISTS AppTokens(' +
              'instance TEXT NOT NULL PRIMARY KEY, ' +
              'id TEXT, ' +
              'name TEXT, ' +
              'redirect_uri TEXT, ' +
              'scopes TEXT, ' +
              'website TEXT, ' +
              'client_id TEXT NOT NULL, ' +
              'client_secret TEXT NOT NULL, ' +
              'vapid_key TEXT' +
          ')'
      )

      query(
          'CREATE TABLE IF NOT EXISTS UserTokens(' +
              'instance TEXT NOT NULL, ' +
              'username TEXT NOT NULL, ' +
              'access_token TEXT NOT NULL, ' +
              'created_at TEXT, ' +
              'expires_in TEXT, ' +
              'id TEXT, ' +
              'me TEXT, ' +
              'refresh_token TEXT, ' +
              'scope TEXT, ' +
              'token_type TEXT, ' +
              'PRIMARY KEY(instance,username)' +
          ')'
      )
      
      setFirstPage(getUserToken())
  }

  /**
   * This drops the content of the Database
   * Usefull for debugging and logging out
   */
  function drop_all() {
    query('DROP TABLE AppTokens')
    query('DROP TABLE UserTokens')
  }
  
  function getUserToken(){
    return first("SELECT instance, username, access_token FROM UserTokens", [], {})
  }
  
  function deleteUserToken(instance) {
    return query("DELETE FROM UserTokens WHERE instance=?", [instance], {})
  }

  function getAppkey(instance) {
    return first("SELECT * FROM AppTokens WHERE instance=?", [instance], {})
  }
  
  function saveAppKey(app_key) {
    log.cli("Storing app_key " + JSON.stringify(app_key), log.level.DEBUG)
    return query(
      'INSERT INTO AppTokens VALUES(?,?,?,?,?,?,?,?,?);', 
        [
          app_key.instance, 
          app_key.id, 
          app_key.name, 
          app_key.redirect_uri, 
          app_key.scopes, 
          app_key.website, 
          app_key.client_id, 
          app_key.client_secret, 
          app_key.vapid_key,
        ]) 
  }
  
  function saveUserToken(user_token) {
    log.cli("Storing user_token " + JSON.stringify(user_token), log.level.DEBUG)
    
    return query(
      'INSERT INTO UserTokens VALUES(?,?,?,?,?,?,?,?,?,?);', 
        [
          user_token.instance,
          user_token.username,
          user_token.access_token,
          user_token.created_at,
          user_token.expires_in,
          user_token.id,
          user_token.me,
          user_token.refresh_token,
          user_token.scope,
          user_token.token_type
        ]) 
  }
  
  /**
   * We can only decide what first page to show after the local storage is loaded, so we start here
   */
  Component.onCompleted: {
    initialise_pui()
  }
}
