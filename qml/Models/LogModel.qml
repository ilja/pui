import QtQuick 2.9
import QtQuick.LocalStorage 2.0

/**
 * Logger Model
 * 
 * This model is responsible for logging
 * 
 */

Item {
  id: log
  
  property var level: {"DEBUG": 0, "INFO": 1, "WARNING": 2, "ERROR": 3}
  
  // TODO Implement a way to easily set the loglevel that's used
  
  function cli(log_message, log_level) {
    let global_log_level = level.ERROR
    
    if (global_log_level <= level.ERROR && log_level === level.ERROR) {
      console.error("❌[Error]", log_message)
    } else if (global_log_level <= level.WARNING && log_level === level.WARNING) {
      console.log("⚠️[WARNING]", log_message)
    } else if (global_log_level <= level.INFO && log_level === level.INFO) {
      console.log("ℹ️[INFO]", log_message)
    } else if (global_log_level <= level.DEBUG && log_level === level.DEBUG) {
      console.log("🐛[DEBUG]", log_message)
    }
  }
}
