import QtQuick 2.9

/**
 * HTTP Model
 * 
 * This model is responsible for http requests
 * 
 * See https://api.pleroma.social/ for the Pleroma API
 * 
 */

Item {
  id: http
    
  property var user_token: undefined
  
  /**
   * Do a request. This is an abstraction and shouldn't be used directly outside of the model
   * Use `get` or `post` instead.
   */
  function _call(type, path, headers, payload, callback_succes, callback_error) {
    let url = "https://" + user_token.instance + path    
    let request = new XMLHttpRequest()
    request.responseType = 'json';
    
    request.onreadystatechange = () => {
      if (request.readyState == XMLHttpRequest.DONE) {
        if (request.status == 200) {
          log.cli(JSON.stringify(request.response), log.level.DEBUG)
          callback_succes(request)
        } else {
          log.cli(request.status + " " + JSON.stringify(request.response), log.level.ERROR)
          callback_error(request)
        }
      }
    }
    request.open(type, url, true); // true for asynchronous
    for (let i=0; i < headers.length; i++) {
      request.setRequestHeader(headers[i][0], headers[i][1])
    }
    log.cli('Calling: ' + url, log.level.INFO)
    log.cli('Type: ' + type, log.level.DEBUG)
    log.cli('Headers: ' + headers, log.level.DEBUG)
    log.cli('Payload: ' + payload, log.level.DEBUG)
    request.send(payload);
  }
  
  /**
   * Do a get request
   */
  function get(path, headers, payload, callback_succes, callback_error) {
    _call("GET", path, headers, payload, callback_succes, callback_error)
  }

  /**
   * Do a post request
   */
  function post(path, headers, payload, callback_succes, callback_error) {
    _call("POST", path, headers, payload, callback_succes, callback_error)
  }
  
  function fetchTimeline(callback_succes, callback_error, since_id, max_id) {
    let path = '/api/v1/timelines/home?limit=20'
    let headers = [
      ["Content-Type", "application/json; charset=utf-8"], 
      ["Accept", "application/json"],
      ["Authorization", "Bearer " + user_token.access_token]
    ]
    let payload = "{}"
    
    if (since_id){
      path += '&since_id=' + since_id
    }
    
    if (max_id){
      path += '&max_id=' + max_id
    }
    
    get(path, headers, payload, (request) => { normalise_timeline(request, callback_succes) }, callback_error)
  }
  
  function normalise_timeline(request, callback_succes) {
    let response = request.response
    let fetched_timeline = []
    
    for (let i=0;i<response.length;i++) {
      fetched_timeline[i] = pleromaPostToPuiModel(response[i])
    }

    callback_succes(fetched_timeline)
  }
  
    function pleromaPostToPuiModel(pleroma_post_data) {
    let post_data = {}
    
    function show_username(is_local, account_username, account_fqn){
      let username
      if (is_local) {
        username = account_username
      } else {
        username = account_fqn
      }
      return username
    }
    
    function in_reply_to_username(post_data) {
      let account = {}
      
      for (let i=0;i < post_data.mentions.length;i++) {
        if (post_data.mentions[i].id === post_data.pleroma.in_reply_to_account_id) {
          return post_data.mentions[i].acct
        }
      }
      
      return post_data.pleroma.in_reply_to_account_acct || ""
    }
    
    function media_height(media_attachement){
      if (media_attachement.is_audio || media_attachement.is_unknown) {
        return units.gu(5)
      } else {
        return units.gu(20)
      }
    }
    
    post_data.id = pleroma_post_data.id
    post_data.is_reblog = (pleroma_post_data.reblog ? true : false)
    post_data.reblog = {}
    post_data.reblog.account = {}
    post_data.reblog.account.avatar = ""
    post_data.reblog.account.username = ""
    post_data.is_in_reply_to = (pleroma_post_data.in_reply_to_id ? true : false)
    post_data.in_reply_to = {}
    post_data.in_reply_to.account = {}
    post_data.in_reply_to.account.username = in_reply_to_username(pleroma_post_data)
    post_data.note = {}
    post_data.note.account = {}
    post_data.note.account.avatar = ""
    post_data.note.account.display_name = ""
    post_data.note.account.username = ""
    post_data.note.content = {}
    post_data.note.content.has_subject = (pleroma_post_data.spoiler_text ? true : false)
    post_data.note.content.subject = pleroma_post_data.spoiler_text
    post_data.note.content.content = pleroma_post_data.content
    post_data.created_at = ""
    post_data.visibility = pleroma_post_data.visibility
    post_data.replies_count = pleroma_post_data.replies_count
    post_data.reblogs_count= pleroma_post_data.reblogs_count
    post_data.favourites_count = pleroma_post_data.favourites_count
    post_data.reblogged = pleroma_post_data.reblogged
    post_data.favourited = pleroma_post_data.favourited
    post_data.has_media = (pleroma_post_data.media_attachments.length > 0 ? true : false)
    post_data.media_attachements = []
    
    if (post_data.has_media) {
      for (let i=0;i<pleroma_post_data.media_attachments.length;i++) {
        let pleroma_attachement = pleroma_post_data.media_attachments[i]
        
        post_data.media_attachements[i] = {
          type: pleroma_attachement.type,
          mime_type: pleroma_attachement.pleroma.mime_type,
          preview_url: pleroma_attachement.preview_url,
          remote_url:pleroma_attachement.remote_url,
          description: pleroma_attachement.description,
          is_image: pleroma_attachement.type === "image",
          is_video: pleroma_attachement.type === "video",
          is_audio: pleroma_attachement.type === "audio",
          is_unknown: pleroma_attachement.type === "unknown"
        }
        
        post_data.media_attachements[i].height = media_height(post_data.media_attachements[i])
        
      } 
    }
    
    if (post_data.is_reblog) {
      post_data.reblog.account.avatar = pleroma_post_data.account.avatar
      post_data.reblog.account.username = show_username(
        pleroma_post_data.pleroma.local,
        pleroma_post_data.account.username,
        pleroma_post_data.account.fqn
      )
      
      post_data.note.account.avatar = pleroma_post_data.reblog.account.avatar
      post_data.note.account.display_name = pleroma_post_data.reblog.account.display_name
      post_data.note.account.username = show_username(
        pleroma_post_data.reblog.pleroma.local,
        pleroma_post_data.reblog.account.username,
        pleroma_post_data.reblog.account.fqn
      )
      post_data.created_at = pleroma_post_data.reblog.created_at
      
    } else {
      post_data.note.account.avatar = pleroma_post_data.account.avatar
      post_data.note.account.display_name = pleroma_post_data.account.display_name
      post_data.note.account.username = show_username(
        pleroma_post_data.pleroma.local,
        pleroma_post_data.account.username,
        pleroma_post_data.account.fqn
      )
      post_data.created_at = pleroma_post_data.created_at
    }
    
    return post_data
  }
  
  function revoke_login() {
    let app_key = store.getAppkey(user_token.instance)
    let path = '/oauth/revoke'
    let headers = [
      ["Content-Type", "application/json; charset=utf-8"],
      ["Accept", "*/*"],
      ["Authorization", "Bearer " + user_token.access_token]
    ]
    let payload = JSON.stringify({
      client_id: app_key.client_id,
      client_secret: app_key.client_secret,
      token: user_token.access_token
    })

    post(
      path,
      headers,
      payload,
      // onSucces
      (request) => {
        log.cli('Logout succesfull', log.level.INFO)
      },
      // onError
      (request) => {
        log.cli('Logout failed', log.level.ERROR)
      }
    )
  }

  function registerUser(app_key, password, labelError) {
    let path = '/oauth/token'
    let headers = [["Content-Type", "application/json; charset=utf-8"], ["Accept", "application/json"]]
    let payload = JSON.stringify({
      client_id: app_key.client_id,
      client_secret: app_key.client_secret,
      redirect_uri: app_key.redirect_uri,
      grant_type: "password",
      username: user_token.username,
      password: password
    })

    post(
      path, 
      headers, 
      payload,
      // onSucces
      (request) => {
        let rq_user_token = request.response
        rq_user_token.username = user_token.username
        rq_user_token.instance = user_token.instance
        
        store.saveUserToken(rq_user_token)
        setFirstPage(rq_user_token)
      },
      // onError
      (request) => {labelError.text = i18n.tr("ERROR: Loggin failed")} // TODO: more precise feedback depending on http status code)
    )
  }
  
  function registerApp(callback_succes, callback_error) {
    // See https://api.pleroma.social/#operation/AdminAPI.OAuthAppController.create
    let path = '/api/v1/apps'
    let headers = [["Content-Type", "application/json; charset=utf-8"], ["Accept", "application/json"]]
    let payload = JSON.stringify({
      client_name: root.application_name,
      redirect_uris: "urn:ietf:wg:oauth:2.0:oob",
      scopes: "read, write, follow, push, admin",
      website: "https://codeberg.org/ilja/pui"
    })
    
    post(path, headers, payload, callback_succes, callback_error)
  }
  
  function authenticate(instance, username, password, labelError) {
      let app_key = store.getAppkey(instance)

      user_token.instance = instance
      user_token.username = username

      // If we don't have an appkey for this instance yet, we try to get one
      // If/when we have one, we try to authenticate
      if (! app_key.client_secret) {
        http.registerApp(
          (request) => {
            app_key = request.response
            app_key.instance = instance
            app_key.scopes = "read, write, follow, push, admin"
            store.saveAppKey(app_key)
            registerUser(app_key, password, labelError)
          },
          (request) => {labelError.text = i18n.tr("ERROR: Couldn't fetch an app key")} // TODO: more precise feedback depending on http status code
        )
      } else {
        registerUser(app_key, password, labelError)
      }
  }
  
  function setUserToken(user_token_obj) {
    user_token = user_token_obj
  }
}
