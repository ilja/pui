# Reading Pui

Media used for the app, e.g. images, are stored in the `assets` folder.

The build folder contains a bunch of stuff needed for building. I'm gonna be honest, I have no clue what most of these do, they came with the basic template. When you build this package, you can find the .click archive at `build/all/app/pui.ilja_1.0.0_all.click`. This is the actual build an what's needed by your phone in order to install Pui.

The `po` folder contains translations. In the code you'll notice that Strings that are shown in the user interface are typically passed through a `i18n.tr()` function. This folder can contain files for different languages. The function takes the string, sees if it can provide a proper transation based on the Ubuntu-Touch language settings and the files it finds in th `po` folder and then either returns the translated version, or the string itself if no translation is found.

The `qml` folder is by far the most important one. Here are the QML source files kept. QML uses components that can be imported and logic can be added using Javascript.

The structure of each .qml file follows a similar patern
    * We start with import statements. We import QtQuick and whatever other components we need. We try to use the Ubuntu Components as much as possible.
    * Then we have the general Component that encaptulates the rest of the page or module.
    * First we set the properties of the module
    * Then we set global variables if there are any
    * Then we'll add functions
    * Lastly we'll add the structure of the visual parts of the page

In the `qml` folder we first find a file `Main.qml`. Here we also define some modules that can be used throughout the application. THese modules don't do anything visualy, but are used to abstract some logic away in logical clusters. We also define a `PageStack` element which helps us to easily navigate through pages.

In the `qml/Pages` folder, we finde the different pages. When you have a certain screen you can see (a login screen, a timeline...), we made these as modules in seperate pages. Here, the general Component is always a `Page`.

In the `qml/Models` folder, we finde the different models. Models are just abstractions to help us make the rest of the code easier to manage and more readable.

Throughout the code commets can be found to help you guide through the code.

## Further reading

* QML docs: <https://doc.qt.io/qt-5/qtqml-index.html>
    * It also has a simple tutorial. Personaly I used <https://snapcraft.io/qmlcreator> to guide me withthe first steps. Note that this will use the default QML components and not the Ubuntu ones.
* Docs for the Ubuntu Components: <https://phone.docs.ubuntu.com/en/apps/api-qml-current/>
* UBPorts Docs on app development: <https://docs.ubports.com/en/latest/appdev/index.html>
* Clickable repo and README: <https://github.com/bhdouglass/clickable>
